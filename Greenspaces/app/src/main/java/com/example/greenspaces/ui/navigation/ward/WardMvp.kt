package com.example.greenspaces.ui.navigation.ward

import com.example.greenspaces.arch.android.BaseMvp
import com.example.greenspaces.arch.android.BasePresenter
import com.example.greenspaces.data.models.Stat

interface WardMvp : BaseMvp {
    interface View : BaseMvp.View {
        fun showWardStat(stats: List<Stat>)
        fun showError()
    }

    interface Presenter : BaseMvp.Presenter{
        fun load()
    }
}