package com.example.greenspaces.ui.navigation.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.charts.Pie
import com.example.greenspaces.R
import com.example.greenspaces.data.models.Stat
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import kotlinx.android.synthetic.main.element_statistic.view.*

class HomeAdapter(var context: Context) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    var stats = mutableListOf<Stat>()

    sealed class Status{
        object Normal: Status()
        object Loading: Status()
        object Error: Status()
    }

    private var status: Status = Status.Loading

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        when(status){
            is Status.Normal -> {
                holder.error.visibility = View.GONE
                holder.statLayout.visibility = View.VISIBLE

                val params = holder.bar.layoutParams ?: LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT)
                (params as LinearLayout.LayoutParams).weight = (stats[position].statistic/100).toFloat()

                holder.bar.layoutParams = params
                holder.title.setText(stats[position].title)
                holder.stat.text = context.getString(R.string.statistic, String.format("%.1f", stats[position].statistic))
                holder.lcl.text = context.getString(R.string.lcl, String.format("%.1f", stats[position].lower_confidence_limit))
                holder.ucl.text = context.getString(R.string.ucl, String.format("%.1f", stats[position].upper_confidence_limit))
                holder.year.text = context.getString(R.string.stat_year, stats[position].year)

//                var data = mutableListOf<PieEntry>()
//                data.add(PieEntry(stats[position].lower_confidence_limit.toFloat()))
//                data.add(PieEntry(stats[position].lower_confidence_limit.toFloat()))
//                data.add(PieEntry(stats[position].lower_confidence_limit.toFloat()))
//
//                var dataSet = PieDataSet(data, "Label")
//                var dataPie = PieData(dataSet)
//                holder.chart.data = dataPie
//                holder.chart.invalidate()

            }
            Status.Loading -> {
                holder.statLayout.visibility = View.GONE
            }
            Status.Error -> {
                holder.error.visibility = View.VISIBLE
            }
        }
    }

    fun showStats(stats: List<Stat>){
        this.stats = stats.toMutableList()
        status = Status.Normal
        notifyDataSetChanged()
    }

    fun showError(){
        Status.Error
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.element_statistic, parent, false))
    }

    override fun getItemCount(): Int {
        return stats.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = view.title
        val error = view.error
        val stat = view.statistic
        val year = view.year
        val statLayout = view.stat_layout
        val bar = view.bar
        val lcl = view.lcl
        val ucl = view.ucl
    }
}