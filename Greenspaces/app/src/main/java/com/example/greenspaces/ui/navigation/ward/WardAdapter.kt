package com.example.greenspaces.ui.navigation.ward

import android.app.ActionBar
import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.icu.util.Measure
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.greenspaces.R
import com.example.greenspaces.data.models.Stat
import com.example.greenspaces.ui.navigation.home.HomeAdapter
import com.example.greenspaces.ui.navigation.home.HomeFragment
import kotlinx.android.synthetic.main.element_statistic.view.*
import java.lang.Exception
import kotlin.math.floor

class WardAdapter(var context: Context) : RecyclerView.Adapter<WardAdapter.ViewHolder>() {

    var stats = mutableListOf<Stat>()
    lateinit var holder: ViewHolder

    sealed class Status{
        object Normal: Status()
        object Loading: Status()
        object Error: Status()
    }

    private var status: Status = Status.Loading

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        this.holder = holder
        when (status) {
            is Status.Normal -> {
                holder.statLayout.visibility = View.VISIBLE
                holder.error.visibility = View.GONE

                val params = holder.bar.layoutParams ?: LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT)
                (params as LinearLayout.LayoutParams).weight = (stats[position].statistic/100).toFloat()

                holder.bar.layoutParams = params
                holder.card.setCardBackgroundColor(ContextCompat.getColor(context, colour(stats[position]).colour))
                holder.title.setText(stats[position].title)
                holder.stat.text = context.getString(R.string.statistic, String.format("%.1f", stats[position].statistic))
                holder.lcl.text = context.getString(R.string.lcl, String.format("%.1f", stats[position].lower_confidence_limit))
                holder.ucl.text = context.getString(R.string.ucl, String.format("%.1f", stats[position].upper_confidence_limit))
                holder.year.text = context.getString(R.string.stat_year, stats[position].ward_name)
            }
            Status.Loading -> {
                holder.statLayout.visibility = View.GONE
            }
            Status.Error -> {
                holder.error.visibility = View.VISIBLE
            }
        }
    }

    fun showStats(stats: List<Stat>){
        this.stats = stats.toMutableList()
        status = Status.Normal
        notifyDataSetChanged()
    }

    fun showError(){
        Status.Error
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.element_statistic, parent, false))
    }

    override fun getItemCount(): Int {
        return stats.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val bar = view.bar
        val card = view.card
        val title = view.title
        val error = view.error
        val stat = view.statistic
        val year = view.year
        val statLayout = view.stat_layout
        val lcl = view.lcl
        val ucl = view.ucl
    }

    private fun colour(stat: Stat): Colour{
        val ll = HomeFragment.statForCity.lower_confidence_limit
        val st = HomeFragment.statForCity.statistic
        val ul = HomeFragment.statForCity.upper_confidence_limit

        if(stat.upper_confidence_limit < ll) return Colour.G0
        if(stat.statistic < ll) return Colour.G1
        if(stat.lower_confidence_limit > ul) return Colour.G4
        if(stat.statistic > st) return Colour.G3
        if(stat.statistic > ll) return Colour.G2
        else return Colour.G2
    }

    enum class Colour(@ColorRes var colour: Int){
        G0(R.color.gradient_0),
        G1(R.color.gradient_2),
        G2(R.color.gradient_4),
        G3(R.color.gradient_7),
        G4(R.color.gradient_9)
    }
}