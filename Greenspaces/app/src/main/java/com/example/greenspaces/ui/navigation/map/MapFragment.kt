package com.example.greenspaces.ui.navigation.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.greenspaces.BuildConfig
import com.example.greenspaces.R
import com.example.greenspaces.arch.android.BaseFragment
import com.example.greenspaces.data.models.Stat
import kotlinx.android.synthetic.main.fragment_map.*

class MapFragment : BaseFragment<MapMvp.Presenter>(), MapMvp.View {

    companion object{
        fun newInstance(): MapFragment {
            return MapFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = MapPresenter(this)
        presenter.load()
    }

    override fun setupMap() {
        web_view.loadUrl(BuildConfig.URL)
        web_view.settings.javaScriptEnabled = true
    }

    override fun finish() {

    }
}