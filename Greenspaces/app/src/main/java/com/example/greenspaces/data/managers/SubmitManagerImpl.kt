package com.example.greenspaces.data.managers

import com.example.greenspaces.data.api.AwsService
import com.example.greenspaces.data.models.Submit
import io.reactivex.Single

class SubmitManagerImpl(var service: AwsService) : SubmitManager {

    override fun newSubmission(submit: Submit): Single<Submit> {
        return service.newSubmit(submit).map { body ->
            body
        }
    }
}