package com.example.greenspaces.ui.navigation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.greenspaces.R
import com.example.greenspaces.arch.android.BaseFragment
import com.example.greenspaces.data.models.Stat
import kotlinx.android.synthetic.main.fragment_ward.*

class HomeFragment : BaseFragment<HomeMvp.Presenter>(), HomeMvp.View {

    companion object{
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
        lateinit var statForCity: Stat
    }

    private lateinit var adapter: HomeAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_stats.layoutManager = LinearLayoutManager(activity)
        recycler_stats.itemAnimator = DefaultItemAnimator()
        recycler_stats.adapter = PlaceholderAdapter()
        presenter = HomePresenter(this)
        presenter.load()
    }

    override fun showFullCityStat(stats: List<Stat>) {
        adapter = HomeAdapter(activity!!)
        recycler_stats.adapter = adapter
        statForCity = stats[1]
        adapter.showStats(stats)
    }

    override fun showError(){
        adapter = HomeAdapter(activity!!)
        recycler_stats.adapter = adapter
        adapter.showError()
    }

    override fun finish() {

    }
}