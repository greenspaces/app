package com.example.greenspaces.data.api

import com.example.greenspaces.BuildConfig
import com.example.greenspaces.arch.AppModule
import com.google.gson.Gson
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiModule {

    private const val API_URL = "https://opendata.bristol.gov.uk/"
    private const val SUBMIT_API = "http://ec2-54-175-115-51.compute-1.amazonaws.com:8080/"
    private const val CACHE_SIZE_BYTES = 1024 * 1024 * 2L

    val apiGreenspacesService: GreenspacesService by lazy {
        retrofitGreen.create(GreenspacesService::class.java)
    }

    val apiAwsService: AwsService by lazy {
        retrofitAws.create(AwsService::class.java)
    }

    private val retrofitGreen by lazy {
        Retrofit.Builder()
            .baseUrl(API_URL)
            .client(okhttp)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    private val retrofitAws by lazy {
        Retrofit.Builder()
            .baseUrl(SUBMIT_API)
            .client(okhttp)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    private val okhttp by lazy {
        OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY))
            .cache(Cache(AppModule.application.cacheDir, CACHE_SIZE_BYTES))
            .build()
    }

    val gson by lazy {
        Gson()
    }
}