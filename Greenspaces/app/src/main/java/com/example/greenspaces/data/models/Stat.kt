package com.example.greenspaces.data.models

import java.io.Serializable

data class Stat(
    val datasetid: String,
    val recordid: String,
    val upper_confidence_limit: Double,
    val title: String,
    val standard_error: Double,
    val sample_size: Int,
    val year: String,
    val lower_confidence_limit: Double,
    val ward_name: String,
    val statistic: Double,
    val theme: String

):Serializable{

    companion object{
        fun fromTemplate(template: StatTemplate):Stat?{

            return Stat(
                template.datasetid,
                template.recordid,
                template.fields.upper_confidence_limit,
                template.fields.indicator,
                template.fields.standard_error,
                template.fields.sample_size,
                template.fields.year.orEmpty(),
                template.fields.lower_confidence_limit,
                template.fields.ward_name,
                template.fields.statistic,
                template.fields.theme
            )
        }
    }
}

data class StatTemplate(
    val datasetid: String,
    val recordid: String,
    val fields: Field,
    val record_timestamp: String
)

data class Wrapper<T>(
    val records: List<T>
)

data class Field(
    val upper_confidence_limit: Double,
    val indicator: String,
    val standard_error: Double,
    val sample_size: Int,
    val year: String?,
    val lower_confidence_limit: Double,
    val ward_name: String,
    val theme: String,
    val statistic: Double
)