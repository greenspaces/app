package com.example.greenspaces.ui.navigation.submit

import com.example.greenspaces.arch.android.BasePresenter

class SubmittedPresenter(submittedView: SubmittedMvp.View) : BasePresenter<SubmittedMvp.View>(submittedView), SubmittedMvp.Presenter {

    override fun load() {
        view?.setClickables()
    }

}