package com.example.greenspaces.ui.navigation.ward

import android.util.Log
import com.example.greenspaces.arch.android.BasePresenter
import com.example.greenspaces.data.DataModule
import com.example.greenspaces.data.managers.StatManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class WardPresenter(welcomeView: WardMvp.View,  private val statManager: StatManager = DataModule.statManager) : BasePresenter<WardMvp.View>(welcomeView), WardMvp.Presenter {

    override fun load(){
        getWardStats()
    }

    fun getWardStats(){
        statManager.getWardStats()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ xd ->
                view?.showWardStat(xd)
            }, { e ->
                view?.showError()
                Log.e("error", "something went wrong trying to load the city wide stats, check the error message: ", e)
            }).addTo(subscription)
    }
}