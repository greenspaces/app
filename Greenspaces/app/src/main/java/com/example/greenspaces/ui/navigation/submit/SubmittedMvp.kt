package com.example.greenspaces.ui.navigation.submit

import com.example.greenspaces.arch.android.BaseMvp

interface SubmittedMvp : BaseMvp {
    interface View : BaseMvp.View {
        fun setClickables()
    }

    interface Presenter : BaseMvp.Presenter {
        fun load()
    }
}
