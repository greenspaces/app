package com.example.greenspaces.ui.main

import android.Manifest
import android.app.Activity
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.LocationListener
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.*
import androidx.fragment.app.Fragment
import com.example.greenspaces.R
import com.example.greenspaces.arch.android.BaseActivity
import com.example.greenspaces.arch.android.BaseFragment
import com.example.greenspaces.ui.navigation.home.HomeFragment
import com.example.greenspaces.ui.navigation.map.MapFragment
import com.example.greenspaces.ui.navigation.submit.SubmitFragment
import com.example.greenspaces.ui.navigation.ward.WardFragment
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainMvp.Presenter>(), MainMvp.View {

    private var fragmentMap = HashMap<String, BaseFragment<*>>()
    private var activeFragment = TAG_HOME_FRAGMENT


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenter(this)
        presenter.onCreate()
    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.top_nav, menu)
//        return true
//    }

    override fun loadTabs() {
        activeFragment = TAG_HOME_FRAGMENT
        navigation.selectedItemId = R.id.menu_home
        swapFragment(homeFragment())

        navigation.setOnNavigationItemSelectedListener { item ->
            var fragment: Fragment?
            fragment = null
            when (item.itemId) {
                R.id.menu_home -> {
                    fragment = homeFragment()
                    activeFragment = TAG_HOME_FRAGMENT
                }
                R.id.menu_ward -> {
                    fragment = wardFragment()
                    activeFragment = TAG_WARD_FRAGMENT
                }
                R.id.menu_map -> {
                    fragment = mapFragment()
                    activeFragment = TAG_MAP_FRAGMENT
                }
                R.id.menu_submit -> {
                    fragment = submitFragment()
                    activeFragment = TAG_SUBMIT_FRAGMENT
                }
            }
            if(fragment != null) swapFragment(fragment)
            true
        }
    }

    override fun createLocationRequest() {
        if(checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun swapFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, fragment, "TAG")
        transaction.commitAllowingStateLoss()
    }

    private fun homeFragment() : Fragment {
        var fragment: Fragment? = fragmentMap[TAG_HOME_FRAGMENT]
        if (fragment == null) {
            fragment = HomeFragment.newInstance()
            fragmentMap[TAG_HOME_FRAGMENT] = fragment
        }
        return fragment
    }

    private fun wardFragment() : Fragment {
        var fragment: Fragment? = fragmentMap[TAG_WARD_FRAGMENT]
        if (fragment == null) {
            fragment = WardFragment.newInstance()
            fragmentMap[TAG_WARD_FRAGMENT] = fragment
        }
        return fragment
    }

    private fun mapFragment() : Fragment {
        var fragment: Fragment? = fragmentMap[TAG_MAP_FRAGMENT]
        if (fragment == null) {
            fragment = MapFragment.newInstance()
            fragmentMap[TAG_MAP_FRAGMENT] = fragment
        }
        return fragment
    }

    private fun submitFragment() : Fragment {
        var fragment: Fragment? = fragmentMap[TAG_SUBMIT_FRAGMENT]
        if (fragment == null) {
            fragment = SubmitFragment.newInstance(this)
            fragmentMap[TAG_SUBMIT_FRAGMENT] = fragment
        }
        return fragment
    }

    companion object {
        private const val TAG_HOME_FRAGMENT = "home"
        private const val TAG_WARD_FRAGMENT = "ward"
        private const val TAG_MAP_FRAGMENT = "map"
        private const val TAG_SUBMIT_FRAGMENT = "submit"
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }
}
