package com.example.greenspaces.data.api

import com.example.greenspaces.data.models.StatTemplate
import com.example.greenspaces.data.models.Wrapper
import io.reactivex.Single
import retrofit2.http.GET

interface GreenspacesService {

    @GET("api/records/1.0/search/?dataset=quality-of-life-2018-19-citywide-trend&facet=indicator&facet=theme&facet=ward_name&refine.indicator=%25+satisfied+overall+with+their+current+accommodation")
    fun getStat(): Single<Wrapper<StatTemplate>>

    @GET("api/records/1.0/search/?dataset=quality-of-life-2018-19-ward&facet=indicator&facet=theme_indicator&facet=ward_name&refine.indicator=%25+owner+occupiers+satisfied+overall+with+their+current+accommodation")
    fun getStatWard(): Single<Wrapper<StatTemplate>>
}