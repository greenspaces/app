package com.example.greenspaces.data.models

import androidx.appcompat.view.menu.SubMenuBuilder
import java.io.Serializable

data class Submit(
//    var id: String,
    var age: String,
    var date: String,
    var height: String,
    var latitude: String,
    var longitude: String,
    var gender: String,
    var info: String
):Serializable{
    companion object{
        fun fromTemplate(template: Template):Submit? {
            return Submit(
//                template.id,
                template.age,
                template.date,
                template.height,
                template.latitude,
                template.longitude,
                template.gender,
                template.info
            )
        }
    }
}

data class Template(
//    var id: String,
    var age: String,
    var date: String,
    var height: String,
    var latitude: String,
    var longitude: String,
    var gender: String,
    var info: String
)