package com.example.greenspaces.ui.navigation.map

import android.util.Log
import com.example.greenspaces.arch.android.BasePresenter
import com.example.greenspaces.data.DataModule
import com.example.greenspaces.data.managers.StatManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class MapPresenter(view: MapMvp.View) : BasePresenter<MapMvp.View>(view), MapMvp.Presenter {

    override fun load() {
        view?.setupMap()
    }
}