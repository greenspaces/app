package com.example.greenspaces.data.managers

import com.example.greenspaces.data.models.Stat
import io.reactivex.Single

class MockStatManagerImpl : StatManager {

    override fun getStats(): Single<List<Stat>> {
        return Single.just(listOf(Stat("perfect-bristol",
            "1",
            100.0,
            "percentage of people happy with housing",
            0.0,
            4500,
            "2019",
            83.0,
            "City of Bristol",
            99.9,
            "Housing")))
    }

    override fun getWardStats(): Single<List<Stat>> {
        return Single.just(listOf(Stat("perfect-bristol",
            "1",
            100.0,
            "percentage of people happy with housing",
            0.0,
            4500,
            "2019",
            83.0,
            "City of Bristol",
            99.9,
            "Housing")))
    }
}