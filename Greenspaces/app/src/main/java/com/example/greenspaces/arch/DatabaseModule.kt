package com.example.greenspaces.arch

import androidx.room.Room
import com.example.greenspaces.data.database.AppDatabase

object DatabaseModule {
    val personDb by lazy {
        db.personDao()
    }
    internal val db by lazy {
        Room.databaseBuilder(AppModule.application, AppDatabase::class.java, "app_db")
            .build()
    }
}