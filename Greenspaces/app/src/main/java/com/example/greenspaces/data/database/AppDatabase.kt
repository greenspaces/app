package com.example.greenspaces.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.greenspaces.data.database.dao.PersonDao
import com.example.greenspaces.data.models.Person

@Database(entities = [Person::class], version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun personDao(): PersonDao
}