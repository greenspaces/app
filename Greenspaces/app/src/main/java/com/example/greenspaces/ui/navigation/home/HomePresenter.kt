package com.example.greenspaces.ui.navigation.home

import android.util.Log
import com.example.greenspaces.arch.android.BasePresenter
import com.example.greenspaces.data.DataModule
import com.example.greenspaces.data.managers.StatManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class HomePresenter(welcomeView: HomeMvp.View, private val statManager: StatManager = DataModule.statManager) : BasePresenter<HomeMvp.View>(welcomeView), HomeMvp.Presenter {

    override fun load() {
        getCityWideStat()
    }

    fun getCityWideStat(){
        statManager.getStats()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ xd ->
                view?.showFullCityStat(xd)
            }, { e ->
                view?.showError()
                Log.e("error", "something went wrong trying to load the city wide stats, check the error message: ", e)
            }).addTo(subscription)
    }
}