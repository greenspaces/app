package com.example.greenspaces.ui.navigation.ward

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.greenspaces.R
import com.example.greenspaces.arch.android.BaseFragment
import com.example.greenspaces.data.models.Stat
import com.example.greenspaces.ui.navigation.home.PlaceholderAdapter
import kotlinx.android.synthetic.main.fragment_ward.*

class WardFragment : BaseFragment<WardMvp.Presenter>(), WardMvp.View {

    companion object{
        fun newInstance(): WardFragment {
            return WardFragment()
        }
    }

    private lateinit var adapter: WardAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_ward, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_stats.layoutManager = LinearLayoutManager(activity)
        recycler_stats.itemAnimator = DefaultItemAnimator()
        adapter = WardAdapter(activity!!)
        recycler_stats.adapter = PlaceholderAdapter()
        presenter = WardPresenter(this)
        presenter.load()
    }

    override fun showWardStat(stats: List<Stat>){
        adapter = WardAdapter(activity!!)
        recycler_stats.adapter = adapter
        adapter.showStats(stats)
    }

    override fun showError(){
        adapter = WardAdapter(activity!!)
        recycler_stats.adapter = adapter
        adapter.showError()
    }


    override fun finish() {

    }
}