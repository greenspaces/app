package com.example.greenspaces.data.managers

import com.example.greenspaces.data.models.Person
import io.reactivex.Single

interface DataManager {

    fun getPerson(id: Int): Single<Person>

    fun insert(person: Person)
}