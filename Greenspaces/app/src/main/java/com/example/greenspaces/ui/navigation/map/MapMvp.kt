package com.example.greenspaces.ui.navigation.map

import com.example.greenspaces.arch.android.BaseMvp
import com.example.greenspaces.arch.android.BasePresenter
import com.example.greenspaces.data.models.Stat

interface MapMvp : BaseMvp {
    interface View : BaseMvp.View {
        fun setupMap()
    }

    interface Presenter : BaseMvp.Presenter{
        fun load()
    }
}