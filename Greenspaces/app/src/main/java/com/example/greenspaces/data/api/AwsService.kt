package com.example.greenspaces.data.api

import com.example.greenspaces.data.models.Submit
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface AwsService {

    @Headers("Content-Type: application/json")
    @POST("persons")
    fun newSubmit(@Body submit: Submit) : Single<Submit>
}