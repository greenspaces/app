package com.example.greenspaces.ui.navigation.submit

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.*
import androidx.fragment.app.FragmentActivity
import com.example.greenspaces.arch.PreferenceModule
import com.example.greenspaces.arch.android.BasePresenter
import com.example.greenspaces.data.DataModule
import com.example.greenspaces.data.managers.DataManager
import com.example.greenspaces.data.managers.StatManager
import com.example.greenspaces.data.managers.SubmitManager
import com.example.greenspaces.data.models.Person
import com.example.greenspaces.data.models.Submit
import com.example.greenspaces.ui.main.MainActivity
import com.f2prateek.rx.preferences2.Preference
import com.google.android.gms.location.FusedLocationProviderClient
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.random.Random

class SubmitPresenter(welcomeView: SubmitMvp.View,
                      private val submitManager: SubmitManager = DataModule.submitManager,
                      private val dataManager: DataManager = DataModule.dataManager,
                      private val dbId: Preference<Int> = PreferenceModule.dbId) : BasePresenter<SubmitMvp.View>(welcomeView), SubmitMvp.Presenter {

    override fun load(){
        view?.setupForm()
        view?.setClickables()
    }

    override fun getLocation(l: FusedLocationProviderClient, a: Activity) {
        l.lastLocation.addOnSuccessListener { location ->
            view?.onLocationReceived(location.latitude, location.longitude)
        }
        l.lastLocation.addOnFailureListener { e ->
            if(checkSelfPermission(a, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(a, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MainActivity.LOCATION_PERMISSION_REQUEST_CODE)
                view?.setClickables()
            }
        }
    }

    override fun submitForm(s: Submit) {
        submitManager.newSubmission(s)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ body ->
                view?.onSubmitSucess()
            }, { e ->
                view?.onSubmitError()
            }).addTo(subscription)
    }

}