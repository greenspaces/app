package com.example.greenspaces.arch

import android.preference.PreferenceManager
import com.f2prateek.rx.preferences2.RxSharedPreferences

object PreferenceModule {

    val dbId by lazy {
        rxSharedPrefs.getInteger("db_id", 10)
    }

    private val sharedPrefs by lazy {
        PreferenceManager.getDefaultSharedPreferences(AppModule.application)
    }

    private val rxSharedPrefs by lazy {
        RxSharedPreferences.create(sharedPrefs)
    }
}