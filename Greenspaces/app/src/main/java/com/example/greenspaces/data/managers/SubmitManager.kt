package com.example.greenspaces.data.managers

import com.example.greenspaces.data.models.Submit
import io.reactivex.Single

interface SubmitManager {
    fun newSubmission(submit: Submit) : Single<Submit>
}