package com.example.greenspaces.data.managers

import com.example.greenspaces.data.api.GreenspacesService
import com.example.greenspaces.data.models.Stat
import io.reactivex.Single

class StatManagerImpl(val service: GreenspacesService) : StatManager {

    override fun getStats(): Single<List<Stat>> {
        return service.getStat().map { response ->
            val stats = ArrayList<Stat>()
            for (stat in response.records){
                stats.add(Stat.fromTemplate(stat)!!)
            }
            stats
        }
    }

    override fun getWardStats(): Single<List<Stat>> {
        return service.getStatWard().map { response ->
            val stats = ArrayList<Stat>()
            for (stat in response.records){
                stats.add(Stat.fromTemplate(stat)!!)
            }
            stats
        }
    }


}