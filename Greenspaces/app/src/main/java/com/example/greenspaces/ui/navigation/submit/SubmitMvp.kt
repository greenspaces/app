package com.example.greenspaces.ui.navigation.submit

import android.app.Activity
import android.content.Context
import android.location.LocationManager
import androidx.fragment.app.FragmentActivity
import com.example.greenspaces.arch.android.BaseMvp
import com.example.greenspaces.arch.android.BasePresenter
import com.example.greenspaces.data.models.Stat
import com.example.greenspaces.data.models.Submit
import com.google.android.gms.location.FusedLocationProviderClient

interface SubmitMvp : BaseMvp {
    interface View : BaseMvp.View {
        fun setupForm()
        fun setClickables()
        fun onLocationReceived(lat: Double, lon: Double)
        fun onSubmitSucess()
        fun onSubmitError()
    }

    interface Presenter : BaseMvp.Presenter{
        fun load()
        fun getLocation(l: FusedLocationProviderClient, a: Activity)
        fun submitForm(s: Submit)
    }
}