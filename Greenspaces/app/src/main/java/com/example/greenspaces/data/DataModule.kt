package com.example.greenspaces.data

import com.example.greenspaces.arch.AppModule
import com.example.greenspaces.arch.DatabaseModule
import com.example.greenspaces.data.api.ApiModule
import com.example.greenspaces.data.managers.DataManagerImpl
import com.example.greenspaces.data.managers.StatManagerImpl
import com.example.greenspaces.data.managers.SubmitManagerImpl

object DataModule {

    val dataManager by lazy {
        DataManagerImpl(AppModule.application, ApiModule.gson, DatabaseModule.personDb)
    }

    val statManager by lazy {
        StatManagerImpl(ApiModule.apiGreenspacesService)
    }

    val submitManager by lazy {
        SubmitManagerImpl(ApiModule.apiAwsService)
    }
}