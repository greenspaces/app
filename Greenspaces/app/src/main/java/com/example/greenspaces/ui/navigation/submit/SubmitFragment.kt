package com.example.greenspaces.ui.navigation.submit

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.location.LocationManager
import android.os.Bundle
import android.os.SystemClock
import android.text.TextUtils
import android.util.AndroidException
import android.util.Log
import android.view.*
import android.view.ViewGroup.*
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.greenspaces.R
import com.example.greenspaces.arch.android.BaseFragment
import com.example.greenspaces.arch.hideKeyboard
import com.example.greenspaces.data.models.Submit
import com.example.greenspaces.ui.main.MainActivity
import com.example.greenspaces.ui.navigation.ward.WardFragment
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_submit.*
import org.joda.time.DateTime
import java.util.concurrent.TimeUnit

class SubmitFragment(var a: Activity) : BaseFragment<SubmitMvp.Presenter>(), SubmitMvp.View {

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    var lat: Double = 0.0
    var lon: Double = 0.0

    companion object{
        fun newInstance(activity: Activity): SubmitFragment {
            return SubmitFragment(activity)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_submit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(a)
        presenter = SubmitPresenter(this)
        presenter.load()
    }

    override fun setupForm() {
        Observable.timer(200, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .map {
                if(!height.text.isNullOrEmpty()){
                    if(height.text.toString().toInt() >= 185 || height.text.toString().toInt() <= 160) {
                        height_warning.visibility = View.VISIBLE
                    }
                }
                if(!age.text.isNullOrEmpty()){
                    if(age.text.toString().toInt() >= 80 || age.text.toString().toInt() <= 20) {
                        age_warning.visibility = View.VISIBLE
                    }
                }
            }
            .subscribeOn(Schedulers.io())
            .subscribe()

        additional_information.setOnEditorActionListener { textView, i, keyEvent ->
            if(i == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard()
                additional_information.clearFocus()
            }
            false
        }
//        height.setOnEditorActionListener { textView, i, keyEvent ->
//            if(i == EditorInfo.IME_ACTION_DONE) {
//                hideKeyboard()
//                height.clearFocus()
//            }
//            false
//        }
//        age.setOnEditorActionListener { textView, i, keyEvent ->
//            if(i == EditorInfo.IME_ACTION_DONE) {
//                hideKeyboard()
//                age.clearFocus()
//            }
//            false
//        }
    }

    private fun submit(s: Submit) {
        text_network_error.visibility = View.GONE
        if(checkRequired()){
            text_error.visibility = View.GONE

            val m = Dialog(context!!, R.style.SubmitTheme)
            m.requestWindowFeature(Window.FEATURE_NO_TITLE)
            m.setCancelable(true)
            m.setContentView(R.layout.dialog_submit)
            m.window!!.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
            m.show()

            val no = m.findViewById(R.id.btNegative) as Button
            val yes = m.findViewById(R.id.btPositive) as Button
            no.setOnClickListener { m.cancel() }
            yes.setOnClickListener {
                m.cancel()
                submit_button.visibility = View.GONE
                submit_progress.visibility = View.VISIBLE
                presenter.submitForm(s)
            }
        } else {
            text_error.visibility = View.VISIBLE
            Observable.timer(50, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .map { scrollview.fullScroll(View.FOCUS_DOWN) }
                .subscribeOn(Schedulers.io())
                .subscribe()
        }
    }

    override fun setClickables() {
        location_button.setOnClickListener {
            coords.visibility = View.GONE
            done.visibility = View.GONE
            presenter.getLocation(fusedLocationClient, a)
        }
        submit_button.setOnClickListener {
            val gender = if(gender_male.isChecked) "Male" else "Female"
            submit(Submit(
                age.text.toString(),
                DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),
                height.text.toString(),
                lat.toString(),
                lon.toString(),
                gender,
                additional_information.text.toString()
            ))
        }
        height.setOnFocusChangeListener { view, b ->
            if(!b) {
                if(height.text.isNullOrEmpty()) {
                    height_warning.visibility = View.GONE
                    height.error = "height is required"
                    return@setOnFocusChangeListener
                }
                if(height.text.toString().toInt() >= 185 || height.text.toString().toInt() <= 160) {
                    height_warning.visibility = View.VISIBLE
                }
                else {
                    height_warning.visibility = View.GONE
                }
            }
        }
        age.setOnFocusChangeListener { view, b ->
            if(!b) {
                if(age.text.isNullOrEmpty()) {
                    age_warning.visibility = View.GONE
                    age.error = "age is required"
                    return@setOnFocusChangeListener
                }
                if(age.text.toString().toInt() >= 80 || age.text.toString().toInt() <= 20) {
                    age_warning.visibility = View.VISIBLE
                }
                else {
                    age_warning.visibility = View.GONE
                }
            }
        }
    }

    override fun onLocationReceived(lat: Double, lon: Double) {
        this.lat = lat
        this.lon = lon
        latitude.text = context!!.getString(R.string.latitude, String.format("%.1f", lat))
        longitude.text = context!!.getString(R.string.longitude, String.format("%.1f", lon))
        coords.visibility = View.VISIBLE
        error.visibility = View.GONE
        done.visibility = View.VISIBLE
    }

    override fun onSubmitSucess() {
        startActivity(Intent(activity, SubmittedActivity::class.java))
        activity!!.finish()
    }

    override fun onSubmitError() {
        submit_progress.visibility = View.GONE
        submit_button.visibility = View.VISIBLE
        text_network_error.visibility = View.VISIBLE
    }

    override fun finish() {

    }

    private fun checkRequired() : Boolean {
        val map = mutableMapOf<View, Boolean>(
            latitude to true,
            height to true,
            age to true,
            gender to true)

        if(lat == 0.0 || lon == 0.0) {
            error.visibility = View.VISIBLE
            map[latitude] = false
        }
        if(TextUtils.isEmpty(height.text)) {
            height.error = "height is required"
            map[height] = false
        }
        if(TextUtils.isEmpty(age.text)) {
            age.error = "age is required"
            map[age] = false
        }
        if(!gender_male.isChecked && !gender_female.isChecked){
            gender_error.visibility = View.VISIBLE
            map[gender] = false
        } else { gender_error.visibility = View.GONE }
        for (bool in map.values){ if(!bool) return false }
        return true
    }
}