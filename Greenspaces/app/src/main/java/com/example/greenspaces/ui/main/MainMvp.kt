package com.example.greenspaces.ui.main

import com.example.greenspaces.arch.android.BaseMvp

interface MainMvp : BaseMvp {

    interface View : BaseMvp.View {
        fun loadTabs()
        fun createLocationRequest()
    }

    interface Presenter : BaseMvp.Presenter {
        fun onCreate()
    }
}