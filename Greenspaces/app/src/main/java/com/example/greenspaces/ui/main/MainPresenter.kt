package com.example.greenspaces.ui.main

import com.example.greenspaces.arch.android.BasePresenter

class MainPresenter(mainView: MainMvp.View) : BasePresenter<MainMvp.View>(mainView), MainMvp.Presenter {

    override fun onCreate() {
        view?.loadTabs()
        view?.createLocationRequest()
    }


}